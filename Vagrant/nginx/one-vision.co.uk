upstream localpool {
    server unix:/run/php/php7.2-fpm.sock max_fails=10 fail_timeout=5 weight=400;
}

server {
    listen 80;
    server_name	one-vision.co.uk;
    return 301 https://one-vision.co.uk$request_uri;
}

server {
    listen 80;
    server_name	*.one-vision.co.uk;
    return 301 https://$host$request_uri;
}

server {
    listen       443 ssl;
    server_name  *.one-vision.co.uk;
    access_log   /var/log/nginx/access.log;
    error_log	 /var/log/nginx/error.log warn;

    gzip on;
    gzip_types text/css text/xml application/x-javascript text/plain;
    client_max_body_size 60m;

    ssl on;
    ssl_certificate		/etc/ssl/private/one-vision.crt;
    ssl_certificate_key	/etc/ssl/private/one-vision.key;
    ssl_ciphers	ALL:!aNULL:!ADH:!eNULL:!SSLv2:!LOW:!EXP:RC4+RSA:!MEDIUM:+HIGH;

    set $rewrite_status "";

    root /var/www/one-vision.co.uk/public;
    index index.php;

    if (!-e $request_filename)
    {
        rewrite ^(.+)$ /index.php?q=$1 last;
    }

    location ~* \.(jpg|jpeg|gif|png|swf|ico|mp3|wav|mov|doc|xls|ppt|docx|pptx|xlsx)$ {
        expires         5d;
    }
    location ~* \.(js|css|htc)$ {
        expires         5d;
    }


    location ~ \.(php|html)$ {
        fastcgi_pass  localpool;
        fastcgi_param SCRIPT_FILENAME  /var/www/one-vision.co.uk/public$fastcgi_script_name;
        fastcgi_param SERVER_NAME $http_host;
        fastcgi_intercept_errors on;
        fastcgi_read_timeout 120;
        fastcgi_next_upstream error timeout invalid_header;
        include        fastcgi_params;
    }


    location ~ /\.ht
    {
        deny all;
    }

}

