#!/usr/bin/env bash

GREEN='\033[32m'
NC='\033[0m' # No Color

mkdir -p /home/ubuntu/

apt-get update

debconf-set-selections <<< 'mysql-server mysql-server/root_password password parola'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password parola'

apt-get install -yq software-properties-common
apt-get install -y language-pack-en-base curl git lsof tree lynx nano rar unrar mysql-server zip unzip
LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php

apt-get update
apt-get -y upgrade

DEBIAN_FRONTEND=noninteractive apt-get -y install php7.2 php7.2-mysql php7.2-memcached php7.2-memcache php7.2-gd php7.2-curl php7.2-cli php7.2-fpm php7.2-mbstring php7.2-xml php7.2-zip php7.2-bcmath


curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

apt-get remove libapache2-mod-php7.2 apache2 

apt-get -y install nginx

rm /etc/nginx/sites-enabled/*
cp /vagrant/Vagrant/nginx/one-vision.co.uk /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/one-vision.co.uk /etc/nginx/sites-enabled/

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=oc.co.uk" -keyout /etc/ssl/private/one-vision.key -out /etc/ssl/private/one-vision.crt

service nginx restart


echo 'sql_mode = "ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"' >> /etc/mysql/mysql.conf.d/mysqld.cnf

service mysql restart

echo "192.168.99.111 one-vision.co.uk" >> /etc/hosts

echo "Creating database"
mysql -u root -pparola -e "create database one_vision_dev"

echo "Creating database user"
mysql -u root -pparola -e "grant all privileges on one_vision_dev.* to one_vision_dev@localhost identified by 'one_vision_dev'"

cd /var/www/one-vision.co.uk
sudo -u www-data composer install
sudo -u www-data php artisan migrate
sudo -u www-data php artisan db:seed

