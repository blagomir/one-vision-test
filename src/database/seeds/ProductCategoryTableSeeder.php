<?php

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \App\Product::all();
        $categories = \App\Category::all();

        foreach ($products as $product) {

            foreach ($categories as $category) {
                $productCategory = new \App\ProductCategory();
                $productCategory->product_id = $product->id;
                $productCategory->category_id = $category->id;
                $productCategory->save();
            }

        }
    }
}
