<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        while ($i < 10) {

            $product = new \App\Product();
            $product->name = 'Random Product '.$i;
            $product->price = rand(1000, 9999);
            $product->URL = 'http://one-vision/product/'.$i;
            $product->save();

            $i++;
        }
    }
}
