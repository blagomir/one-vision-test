<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        while ($i < 5) {

            $category = new \App\Category();
            $category->name = 'RandCat '.$i;
            $category->save();

            $i++;
        }

    }
}
