<?php

Route::get('/', 'HomeController@index');
Route::get('/category/{id}', 'HomeController@category');


Route::prefix('admin')->group(function () {
    Route::get('/product', 'AdminController@index');
    Route::get('/product/{id}', 'AdminController@view');
    Route::post('/product/{id}', 'AdminController@save')->name('product.save');
    Route::post('/product', 'AdminController@create');
});