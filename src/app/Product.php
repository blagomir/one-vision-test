<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function categories()
    {
        return $this->hasMany(ProductCategory::class, 'product_id', 'id');
    }
}
