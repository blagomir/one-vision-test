<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{

    public function index(Request $request)
    {
        return view('admin/list')
            ->with('products', Product::all());
    }

    public function view(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $productCategories = ProductCategory::where('product_id', $product->id)->get()->keyBy('category_id')->toArray();

        return view('admin/edit')
            ->with('categories', Category::all())
            ->with('productCategories', array_keys($productCategories))
            ->with('product', $product);
    }

    public function save(Request $request, $id) {
        $product = Product::findOrFail($id);
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->URL = $request->input('URL');
        $product->save();

        ProductCategory::where('product_id', $product->id)->delete();

        foreach ($request->input('category') as $categoryId => $v) {
            $pc = new ProductCategory();
            $pc->product_id = $product->id;
            $pc->category_id = $categoryId;
            $pc->save();
        }

        return Redirect::to('/admin/product/'.$product->id);
    }
}
