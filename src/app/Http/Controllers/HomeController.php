<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        return view('welcome')
            ->with('products', Product::all());
    }

    public function category(Request $request, $id)
    {
        $category = Category::findOrFail($id)->increment('clicks');
        $categoryProducts = ProductCategory::where('category_id', $id)->get()->keyBy('product_id')->toArray();

        return view('welcome')
            ->with('products', Product::whereIn('id', $categoryProducts)->get());
    }
}
