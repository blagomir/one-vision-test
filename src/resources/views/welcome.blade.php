@extends('layout')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">

                @foreach ($products as $product)
                    <div class="col-md-4">
                        <div class="card mb-4 shadow-sm">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                            <div class="card-body">
                                <p class="card-text">{{ $product->name }}</p>
                                <p class="card-text">{{ $product->URL }}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <small class="text-muted">&pound;{{ number_format($product->price) }}</small>
                                </div>
                                <div class="d-flex justify-content-between align-items-center">
                                    <small class="text-muted">
                                        <u>Categories:</u>
                                        <ul>
                                        @foreach ($product->categories as $category)
                                            <li><a href="/category/{{ $category->category_id }}">{{ $category->category->name }}</a> ({{ $category->category->clicks }} clicks)</li>
                                        @endforeach
                                        </ul>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@append