@extends('layout')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">

            <table class="table">
                <thead>
                <th>Product</th>
                <th>Options</th>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td><a href="/admin/product/{{ $product->id }}">edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@append