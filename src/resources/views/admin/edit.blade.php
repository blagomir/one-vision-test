@extends('layout')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">

            <h1>Editing product {{ $product->name }}</h1>

            @include('admin/form')

        </div>
    </div>
@append