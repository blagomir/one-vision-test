{{ Form::model($product, ['route' => ['product.save', $product->id], 'method' => 'post']) }}


Name: {!! Form::text('name', $product->name) !!}<br />
Price: {!! Form::number('price', $product->price) !!}<br />
URL: {!! Form::text('URL', $product->URL) !!}<br />

Categories: <br />

@foreach ($categories as $category)
    {{ $category->name }}: {!! Form::checkbox('category['.$category->id.']', '1', (in_array($category->id, $productCategories) ? true : false)) !!}<br />
@endforeach

<button type="submit" value="1" class="btn btn-primary">Save</button>

{!! Form::close() !!}