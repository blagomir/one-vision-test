####Please read carefully before continuing. 
This simple guide will tell you how to run the demo on your local machine.
This guide assumes you have Vagrant installed on your machine.

In your "hosts" (/etc/hosts for Mac and UNIX) add this:

`192.168.33.111 one-vision.co.uk www.one-vision.co.uk`

Navigate to the project root folder and type

`vagrant up`


Depending on your internet connection it takes 2-3 min for the virtual box to boot, download, install and configure all required software:

1. nginx
2. php-fpm
3. mysql server 5.7
4. self-signed SSL certificate for the website (keep in mind this will produce security alert when you later on open the website in a browser)


The installer will also run `composer install` and will seed the database with the demo data already added to the Laravel seeders.


Go to your browser and open "one-vision.co.uk"